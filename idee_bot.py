import discord
from discord.ext import commands
from discord import File

with open ("help.txt", "r", encoding="utf-8") as f:
    help=f.read()

with open ("idee-info.txt", "r", encoding="utf-8") as f:
    info=f.read()

saved_decks = 'saved_decks.json'  # where to save the decks if needed?

bot = commands.Bot(command_prefix=['i!', 'I!'], description="A Bot to draw idee! cards. Press i!h for help", help=help)

class EmptyDeckError(Exception):
    """Raised when there are no cards on the deck"""
    pass

import random
class Deck:
    def __init__(self, name, path, size, decktype, user=None, cards=None, last_card=0, last_card_rotated=0):
        self.name = name # name of the deck
        self.path = path # where are the images? Two per cards, one of them rotated, 0 is title
        self.size = size # how many cards are in the deck?
        self.user = str(user) # who created the deck?
        self.decktype = decktype # a: Ad Astra, s: Szenario, w: W7
        # create and shuffle deck
        self.cards=cards
        if not cards:
            self.cards = list(range(1,self.size+1)) # cards by numbers in the deck
            random.shuffle(self.cards)
        self.last_card=last_card
        self.last_card_rotated=last_card_rotated

    def encode(self):
        return dict(
            name=self.name,
            path=self.path,
            size=self.size,
            decktype=self.decktype,
            user=self.user,
            cards=self.cards,
            last_card=self.last_card,
            last_card_rotated=self.last_card_rotated
            )

    def set_user(self,user):
        self.user=str(user)

    def shuffle(self):
        self.last_card = 0
        self.cards = list(range(1,self.size+1))
        random.shuffle(self.cards)
        return

    def draw(self):
        if not self.cards:
            raise EmptyDeckError
        self.last_card = self.cards.pop()
        # rotate card
        self.last_card_rotated = random.choice([0, self.size])
        # set bit for new card
        return File(self.path % (self.last_card + self.last_card_rotated))

    def info(self):
        if self.last_card==0:
            lc = "noch keine"
        else:
            lc = f'Karte Nr. {self.last_card}'
            if self.last_card_rotated !=0:
                lc += " (verkehrt)"
        message = f'Deck: {self.name}, zuletzt gezogen: {lc}, bisher gezogen: {self.size - len(self.cards)}/{self.size}.'
        if len(self.cards) == 0:
            message += '\nAlle Karten wurden gespielt. Mit "i!d m" kannst du neu mischen.'
        return message

    def title(self):
        try:
            return File(self.path % 0)
        except FileNotFoundError:
            return

    def rotate(self):
        if self.last_card == 0:
            return
        if self.last_card_rotated == 0:
            return File(self.path % (self.last_card + self.size))
        else:
            return File(self.path % (self.last_card))

    def last(self):
        if self.last_card > 0:
            return File(self.path % (self.last_card + self.last_card_rotated))

from copy import deepcopy
from datetime import timedelta, datetime
from sys import getsizeof
import json

class DeckEncoder(json.JSONEncoder):
    def default(self, z):
        if isinstance(z, Deck):
            return z.encode()
        if isinstance(z, datetime):
            return str(z)
        else:
            return super().default(z)

class Decks():
    def __init__(self):
        self.decks=dict()
        self.last_decks=dict()
        self.life_signs = dict()  # when did the user interact with us last
        self.deck_templates = dict(
            s = Deck(
                name='idee! Szenario',
                size=42,
                path="szenario/szenario_%02d.jpg",
                decktype="s",
                ),
            a = Deck(
                name='idee! Ad Astra',
                size=42,
                path="adastra/adastra_%02d.jpg",
                decktype="a",
            ),
            w = Deck(
                name='idee! W7',
                size=7,
                path="w7/w7_%02d.jpg",
                decktype="w",
            ))

    def show_decks(self):
        result = f'{len(self.decks)} User.\n'
        for user in self.decks:
            result += f'{user}:\n'
            for deck in self.decks[user]:
                result += f'  - {self.decks[user][deck].info()}\n'
        return result

    def save(self, write_file):
        """Write decks to JSON file"""
        data = dict(
            allusers=str(self.decks.keys()),
            decks=self.decks,
            last_decks=self.last_decks,
            life_signs=self.life_signs
            )
        with open(write_file, "w") as wf:
            json.dump(data, wf, cls=DeckEncoder)

    def load(self, load_file):
        """Load decks from JSON file"""
        with open(load_file, "r") as lf:
            data = json.load(lf)
        if data:
            # read decks
            new_decks = {}
            for userstr in data['decks']:
                new_decks[userstr] = {}
                for decktype in data['decks'][userstr]:
                    d = data['decks'][userstr][decktype]
                    deck = Deck(
                        d.get('name'),
                        d.get('path'),
                        d.get('size'),
                        d.get('decktype'),
                        user = userstr,
                        cards = d.get('cards'),
                        last_card = d.get('last_card'),
                        last_card_rotated = d.get('last_card_rotated'))
                    new_decks[userstr][decktype]=deck
            self.decks = new_decks
            # read lst decks
            self.last_decks={}
            for userstr in data['last_decks']:
                decktype = data['last_decks'][userstr]['decktype']
                self.last_decks[userstr] = self.decks[userstr][decktype]
            # read life signs
            new_life_signs = {}
            for entry in data['life_signs']:
                new_life_signs[entry]=datetime.strptime(data['life_signs'][entry], '%Y-%m-%d %H:%M:%S.%f')
            self.life_signs = new_life_signs

    def new_deck(self, user, decktype):
        userstr = str(user)
        newdeck = deepcopy(self.deck_templates[decktype])
        newdeck.shuffle()
        newdeck.set_user(userstr)

        if not self.decks.get(userstr):
            self.decks[userstr] = {}
        self.decks[userstr][decktype]=newdeck

        self.last_decks[userstr] = newdeck
        self.life_signs[userstr] = datetime.now()
        return f'Hi {user.name}. Ich habe dir ein neues {newdeck.name}-Deck angelegt und auch schon gründlich gemischt.'

    def get_deck(self, user, decktype):
        userstr = str(user)
        self.life_signs[userstr] = datetime.now()
        if not self.decks.get(userstr):
            self.new_deck(user, decktype)
        if not self.decks[userstr].get(decktype):
            self.new_deck(user, decktype)
        self.last_decks[userstr] = self.decks[userstr][decktype]
        return self.decks[userstr][decktype]

    def get_last_deck(self, user):
        userstr = str(user)
        self.life_signs[userstr] = datetime.now()
        if not self.last_decks.get(userstr):
            self.new_deck(user,'s')
        return self.last_decks[userstr]

    def get_info(self, user):
        userstr = str(user)
        self.life_signs[userstr] = datetime.now()
        if userstr not in self.decks:
            self.new_deck(user,'s')
            #message = f"{user.name}, ich habe noch keine Decks für dich angelegt. Ein neues Deck machst du mit i!d n auf, die Hilfe gibt's mit i!h"
        message = f"{user.name}, ich habe folgende Decks für dich am Start: \n"
        for deck in self.decks[userstr].values():
            message += f" - {deck.name} ({deck.size - len(deck.cards)}/{deck.size})\n"
        message += f'Dein aktives Deck ist {self.last_decks[userstr].name}. Wechseln mit i!d i [a|s|w].'
        return message

    def clear(self, user):
        userstr = str(user)
        self.life_signs[userstr] = datetime.now()
        del self.decks[userstr]
        del self.last_decks[userstr]
        return f'{user.name}, ich habe alle deine Decks entfernt.'

    def purge(self, days): # check for in active users and delete their decks
        delta = timedelta(days=days)
        if days > 1:
            old_users = []
            for userstr in self.life_signs:
                if (datetime.now() - self.life_signs[userstr]) > delta:
                    del self.decks[userstr]
                    del self.last_decks[userstr]
                    old_users.append(userstr)
            for userstr in old_users:
                del self.life_signs[userstr]

    def purge_info(self):
        maxdays = 0
        now = datetime.now()
        for userstr in self.life_signs:
            maxdays = max((now - self.life_signs[userstr]).days,maxdays)
        decksize = getsizeof(self.decks)
        message = f"""
Ich merke mir gerade Decks für insgesamt {len(self.decks)} User.
Manche User haben seit {maxdays} Tagen nicht mehr mit mir geredet.
Insgesamt benötigen die Decks zur Zeit {decksize} Bytes Platz.
"""
        return message



# Global state
decks = Decks()
try:
    decks.load(saved_decks)
except:
    pass

# Handles messages
@bot.event
async def on_ready():
    print('Hallo! Ich bin %s %s, dein freundlicher Idee!-Bot. Mit i!h verrate ich dir, was ich kann.' % (bot.user.name, bot.user.id))

@bot.command()
async def h(ctx):
    global help
    try:
        with open ("help.txt", "r", encoding="utf-8") as f:
            help=f.read()
    except:
        pass
    await ctx.send(help)

@bot.command()
async def i(ctx):
    global info
    try:
        with open ("idee-info.txt", "r", encoding="utf-8") as f:
            info=f.read()
    except:
        pass
    await ctx.send(info)

@bot.command()
async def k(ctx, *args):
    global decks
    deck = None
    message = f'{ctx.author.name} hat folgende Karte gezogen:'
    # i!k
    if len(args) == 0:
        deck = decks.get_last_deck(ctx.author)
    # i!k a
    elif args[0] in ['a', 's', 'w']:
        deck = decks.get_deck(ctx.author, args[0])
    # i!k numbers - multiple cards
    elif args[0] in ['2','3','4']:
        message = f'{ctx.author.name} hat folgende Karten gezogen:'
        if len(args) > 1 and len(args[1]) > 1:
            message = args[1]
        no_cards=int(args[0])
        deck = decks.get_last_deck(ctx.author)
        files = []
        try:
            for i in range(no_cards):
                files.append(deck.draw())
        except EmptyDeckError:
            await ctx.send(f'{ctx.author.name}, {deck.info()}')
        await ctx.send(message,files=files)
        return
    # i!k message
    elif len(args[0]) > 1:
        deck = decks.get_last_deck(ctx.author)
        message = args[0]
    else:
        await ctx.send(f'{ctx.author.name}, leider habe ich das nicht verstanden. Hilfe bekommst du mit i!h')
        return
    try:
        await ctx.send(message,file=deck.draw())
    except EmptyDeckError:
        await ctx.send(f'{ctx.author.name}, {deck.info()}')
    except:
        await ctx.send(f'{ctx.author.name}, leider kann ich diese Karte (noch) nicht anzeigen. Zur Zeit kann ich nur Szenario-Karten.')

@bot.command()
async def r(ctx):
    global decks
    try:
        deck = decks.get_last_deck(ctx.author)
    except KeyError:
        await ctx.send(f"{ctx.author.name}, ich habe leider noch kein Deck für dich gefunden, von dem ich die letzte Karte rotieren könnte. Ein neues Deck machst du mit i!d n auf, die volle Hilfe gibt's mit i!h")
        return
    f = deck.rotate()
    if f:
        await ctx.send(f'{ctx.author.name}, so sieht deine letzte Karte rotiert aus:',file=f)
    else:
        await ctx.send(f"{ctx.author.name}, du must erst eine Karte ziehen (z. B. mit i!k) bevor du sie rotieren kannst.")

@bot.command()
async def l(ctx):
    global decks
    try:
        deck = decks.get_last_deck(ctx.author)
    except KeyError:
        await ctx.send(f"{ctx.author.name}, ich habe leider noch kein Deck für dich gefunden. Ein neues Deck machst du mit i!d n auf, die volle Hilfe gibt's mit i!h")
        return
    f = deck.last()
    if f:
        await ctx.send(f"{ctx.author.name}, hier ist nochmal deine zuletzt gezogene Karte:", file=f)
    else:
        await ctx.send(f"{ctx.author.name}, du must erst eine Karte ziehen (z. B. mit i!k) bevor du sie dir nochmal ansehen kannst.")

@bot.command()
async def d(ctx, *args):
    global decks
    deck = None

    #await ctx.send(f'Parameter: {args}, Anzahl: {len(args)}')

    if len(args) == 0: # no arguments, we just show some help.
        await ctx.send(decks.get_info(ctx.author))
        return

    if len(args) == 1: # use last deck or "s" in case of "new"
        if args[0]=='n':
            await ctx.send(decks.new_deck(ctx.author, 's'))
            return
        elif args[0]=='c': # clear all
            await ctx.send(decks.clear(ctx.author))
            return
        else:
            deck = decks.get_last_deck(ctx.author)

    if len(args) == 2: # now we have a decktype as second parameter to determine the deck
        decktype = args[1]
        if decktype not in ['s', 'a', 'w']:
            await ctx.send(f"{ctx.author.name}, so ein Deck haben wir nicht. Die Hilfe gibt's mit i!h")
            return
        if args[0]=='n': # New and done!
            await ctx.send(decks.new_deck(ctx.author, decktype))
            return
        else:
            deck = decks.get_deck(ctx.author, decktype)

    if not deck:
        deck = decks.get_last_deck(ctx.author)

    if deck:
        if args[0]=='m': # Mischen!
            deck.shuffle()
            await ctx.send(f'{ctx.author.name}, ich habe dein {deck.name}-Deck neu gemischt.')
            return
        if args[0]=='i': # Info zeigen!
            await ctx.send(f'{ctx.author.name}: {deck.info()}', file=deck.title())
            return

    # if we reached this point, we don't know what to do.
    await ctx.send(f"{ctx.author.name}, das habe ich nicht verstanden. Ein neues Deck machst du mit i!d n auf, die Hilfe gibt's mit i!h")

@bot.command()
async def admin(ctx,*args):
    global decks
    with open('admin.txt') as f:
        ADMIN_USER = f.readline()
    global decks
    if str(ctx.author) != ADMIN_USER:
        await ctx.send(f'{ctx.author.name}, du bist nicht mein Admin.')
        return

    if not args:
        await ctx.send("""
i!admin info          ... Wie schlimm isses?
i!admin purge [$days]   ... Aufräumen! (Default: 7 $days, Minimum: 1 $days)
i!admin save [$filename]     ... speichert Deck-Data
i!admin load [$filename]     ... lädt Deckdata
i!quit                 ... Ich muss mal weg, ändere Status""")
        return

    if args[0] == 'info':
        await ctx.send(decks.purge_info())
        return

    if args[0] == 'purge':
        if len(args) == 1:
            days = 7
        else:
            days = int(args[1])
        await ctx.send('Vorher: \n' + decks.purge_info())
        await ctx.send(decks.purge(days))
        await ctx.send('Jetzt: \n' + decks.purge_info())
        return

    if args[0] == 'quit':
        await bot.change_presence(status=discord.Status.offline)
        decks.save(saved_decks)
        await ctx.send("""OK, dann bin ich mal weg.""")
        quit()

    if args[0] == 'save':
        save_file = saved_decks if len(args) == 1 else args[1]
        decks.save(save_file)
        await ctx.send(f'{save_file} geschrieben.\n' + decks.purge_info())

    if args[0] == 'load':
        load_file = saved_decks if len(args) == 1 else args[1]
        decks.load(load_file)
        await ctx.send(f'{load_file} geladen.\n' + decks.purge_info())

    if args[0] == 'show':
        await ctx.send(f'{decks.show_decks()}')

    if args[0] == 'test':
        await ctx.send(f'Dein aktives Deck: {decks.last_decks[str(ctx.author)].name}')

from discord.ext.commands import CommandNotFound

@bot.event
async def on_command_error(ctx, error):
    global help
    try:
        with open ("help.txt", "r", encoding="utf-8") as f:
            help=f.read()
    except:
        pass
    if isinstance(error, CommandNotFound):
        await ctx.send(f"{ctx.author.name}, das habe ich nicht verstanden. Hier ist meine Hilfe-Seite (gibt's mit i!h):\n\n" + help)
    else:
        raise error

# Start the bot
with open('token.txt') as f:  # token.txt contains only the secret token.
    TOKEN = f.readline()
bot.run(TOKEN)
