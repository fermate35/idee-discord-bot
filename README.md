# Discord Bot

Ein Bot für Discord, um Idee!-Karten zu ziehen.

Hinweis: Dieses OpenSource-Repository enthält nur Platzhalter für die Karten.
Die tatsächlichen Bilder sind in dem Bot hinterlegt, den du mit dieser URL auf deinen Server einladen kannst:

https://discord.com/api/oauth2/authorize?client_id=809451014305874000&permissions=51200&scope=bot

Als Discord-User kannst du auf den Discord-Server "ideeBot Homebase" kommen und dort testen:

https://discord.gg/fdRXsEcNzz


## Verwendung
```i!k zieht eine Karte vom aktiven Deck, i!h zeigt die Hilfe.```

Ausführlicher:

Du hast deine eigenen Kartenstapel (Decks). Folgende Spiele werden derzeit unterstützt:
```
a ... idee! Ad Astra
s ... idee! Szenario
w ... idee! W7
```
Wenn kein Deck angegeben wird, wird das zuletzt angesehene Deck verwendet (=aktives Deck).

Kommandos für Decks:
```
i!d            ... Welche Decks gibt es? Legt ein idee! Szenario-Deck an, wenn es noch keines gibt.
i!d n [a|s|w]  ... Neues Deck mit [Ad Astra | Szenario | W7] für dich anlegen
i!d m [a|s|w]  ... Dein Deck mischen: [Ad Astra | Szenario | W7]
i!d i [a|s|w]  ... Information zu deinem [Ad Astra | Szenario | W7] Deck anzeigen damit aktives Deck wechseln
i!d c          ... Alle Decks für dich entfernen
```

Kommandos für Karten:
```
i!k            ... Karte vom aktiven Deck ziehen und anzeigen (erzeugt neues Szenario-Deck wenn nötig)
i!r            ... rotiert die zuletzt gezogene Karte des aktiven Decks
i!l            ... zeigt die zuletzt gezogene Karte des aktiven Decks nochmal an
i!k [a|s|w]    ... Karte vom angegebenen Deck ziehen: [Ad Astra | Szenario | W7]
i!k "Text:"    ... Karte ziehen und mit dieser Überschrift anzeigen
i!k [2|3|4]	   ... 2, 3 oder 4 Karten ziehen
```

Allgemeines:
```
i!i           ... Informationen zur Verwendung von idee!
```

Echt jetzt? --> https://shop.flyinggames.de/produkt-kategorie/idee-universalrollenspiel/
